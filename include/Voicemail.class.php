<?php
require_once('BaseAastra.class.php');
require_once('VoicemailListManager.class.php');
require_once('AastraIPPhoneExecute.class.php');
require_once('TextScreen.class.php');

class Voicemail extends BaseAastra {

	var $mapping = array (
		'INBOX' => 'new messages',
		'Old' => 'old messages',
		'Work' => 'work messages',
		'Family' => 'family messages',
		'Friends' => 'friends messages',
	);

	var $mailbox;
	var $path = '/var/spool/asterisk/voicemail/default'; #fixme
	var $box;
	var $timezone = 'America/Los_Angeles';

	private function __readbox($box) {
		$this->box[$box] = array();

		if (!$box) {
			return;
		}

		if (!file_exists($this->path.'/'.$box)) {
			return;
		}

		$d = dir($this->path.'/'.$box);
		if (!$d) {
			return;
		}
		while (false !== ($entry = $d->read())) {
			if (preg_match('/(msg.*)\.(\w+)$/', $entry, $m)) {
				if (!isset($this->box[$box][$m[1]])) {
					$this->box[$box][$m[1]] = array();
				}
				$this->box[$box][$m[1]][] = $m[2];
			}
		}
		$d->close();
	}

	private function __initvar() {
		$this->mailbox = $this->asm->database_get('mboxmap', $this->user);
		if ($this->mailbox == '') {
			throw new Exception('You have no configured mailbox');
		}
		$this->path .= '/'.$this->mailbox;
		$this->box = array();
		foreach($this->mapping as $k => $v) {
			$this->__readbox($k);
		}
		$this->title = 'Voicemail Box '.$this->mailbox;
		date_default_timezone_set($timezone);
	}

	function __construct() {
		parent::__construct();
		$this->__initvar();
	}

	function lock($box) {
		if (!$box) {
			return;
		}
		while (!($f = fopen($this->path.'/'.$box.'/.lock', 'x'))) {
			sleep(1);
		}
		fclose($f);
	}

	function unlock($box) {
		if (!$box) {
			return;
		}
		unlink($this->path.'/'.$box.'/.lock');
	}

	function get_channel() {
		$channel = null;
		for ($i = 0; !$channel; $i++) {
			$channel = $this->asm->database_get('vmail','chan-'.$this->user);
			if (!$channel) {
				$this->asm->wait_event(1);
			}
			if ($i > 10) {
				throw new Exception("Timeout waiting for voicemail to play");
			}
		}
		return $channel;
	}

	function locked_move($src, $msg, $dst) {
		$this->lock($src);
		$this->lock($dst);

		$this->__readbox($src);
		$this->__readbox($dst);

		$x = sprintf("msg%04d", $msg);
		if ($dst) {
			$i = count($this->box[$dst]);
			$y = sprintf("msg%04d", $i);
			foreach ($this->box[$src][$x] as $ext) {
				rename($this->path.'/'.$src.'/'.$x.'.'.$ext,
				       $this->path.'/'.$dst.'/'.$y.'.'.$ext);
			}
		} else {
			foreach ($this->box[$src][$x] as $ext) {
				unlink($this->path.'/'.$src.'/'.$x.'.'.$ext);
			}
		}

		for ($i = $msg + 1; $i < count($this->box[$src]); $i++) {
			$x = sprintf("msg%04d", $i);
			$y = sprintf("msg%04d", $i - 1);
			foreach ($this->box[$src][$x] as $ext) {
				rename($this->path.'/'.$src.'/'.$x.'.'.$ext,
				       $this->path.'/'.$src.'/'.$y.'.'.$ext);
			}
		}
		$this->__readbox($src);
		$this->__readbox($dst);
		$this->unlock($dst);
		$this->unlock($src);		
	}

	##
	# buttons for various states
	##
	function button_play($box, $msg) {
		$this->do->addSoftkey(1, 'Play', $this->url.'?action=message&box='.$box.'&msg='.$msg.'&state=play');
	}

	function button_stop($box, $msg) {
		$this->do->addSoftkey(1, 'Stop', $this->url.'?action=message&box='.$box.'&msg='.$msg.'&state=stop');
	}

	function button_rest($box, $msg) {
		$this->do->addSoftkey(2, 'Delete', $this->url.'?action=message&box='.$box.'&msg='.$msg.'&state=delete');
		$this->do->addSoftkey(3, 'Move', $this->url.'?action=message&box='.$box.'&msg='.$msg.'&state=move');
	}


	##
	# State diagram of voicemail play
	##

	function play_override($box, $msg, $i) {
		$this->asm->database_put('vmail', $this->user, $this->path.'/'.$box.'/'.$msg);
		$this->displayObject( new AastraIPPhoneExecute());
		$this->do->addEntry('Dial:vmail');
		$this->do->addEntry($this->url.'?action=message&box='.$box.'&msg='.$msg.'&state=playing');
		$this->do->setTriggerDestroyOnExit();
		$this->setActionHandler('onhook',$this->url.'?action=message&box='.$box.'&msg='.$msg.'&state=stopped');
	}

	function stop_override($box, $msg, $i) {
		$channel = $this->get_channel();
		$this->displayObject( new AastraIPPhoneExecute());
		# nop action: next transition done by onhook event
		$this->do->addEntry(' ');
		$this->do->setTriggerDestroyOnExit();
		$err = $this->asm->hangup($channel);
	}

	function playing($box, $msg, $i) {
		$this->title = 'Playing Voicemail '.$box.' Message: '.$i;
		$this->do->setAllowDrop();
		$this->button_stop($box, $msg);
		$this->button_rest($box, $msg);
	}

	function stopped_override($box, $msg, $i) {
		$this->delActionHandler('onhook');
		$this->displayObject( new AastraIPPhoneExecute());
		$this->do->addEntry($this->url.'?action=message&box='.$box.'&msg='.$msg);
		$this->do->setTriggerDestroyOnExit();
	}

	function message_start($box, $msg, $i) {
		$this->title = 'Message '.$i.' in mailbox '.$box;
		$this->button_play($box, $msg);
		$this->button_rest($box, $msg);
	}

	function delete($box, $msg, $i) {
		$this->do->addSoftkey(1, 'Yes', $this->url.'?action=message&box='.$box.'&msg='.$msg.'&state=really_delete');
		$this->do->addSoftkey(2, 'No', $this->url.'?action=message&box='.$box.'&msg='.$msg);
		$this->title = 'Delete Message: '.$i.' in mailbox '.$box.'?';
	}

	function really_delete_override($box, $msg, $i) {
		$this->displayObject( new AastraIPPhoneExecute());
		$this->do->addEntry($this->url.'?action=message&box='.$box.'&msg='.$msg);
		$this->do->setTriggerDestroyOnExit();
		$this->locked_move($box, $i - 1, null);
	}

	function move($box, $msg, $i) {
		$this->title = 'Move Message: '.$i.' in mailbox '.$box.'to which mailbox?';
		$i = 1;
		foreach($this->mapping as $k => $v) {
			$this->do->addSoftkey($i++, $v, $this->url.'?action=message&box='.$box.'&msg='.$msg.'&state=move_to&dst='.$k);
		}
	}

	function move_to_override($box, $msg, $i) {
		if (!isset($_GET['dst'])) {
			return;
		}
		$this->displayObject( new AastraIPPhoneExecute());
		$this->do->addEntry($this->url.'?action=message&box='.$box.'&msg='.$msg);
		$this->do->setTriggerDestroyOnExit();
		$this->locked_move($box, $i - 1, $_GET['dst']);
	}

	function message() {
		$box = $_GET['box'];
		if (count($this->box[$box]) == 0) {
			$this->displayObject(new TextScreen());
			$this->title = 'Mailbox '.$box;
			$this->do->setText('is empty');
			$this->back = $this->url;
			return;
		}
		$msg = $_GET['msg'];
		preg_match('/^msg(\d*)$/', $msg, $m);
		$i = intval($m[1]) + 1;
		if (!isset($this->box[$box][$msg])) {
			$i = count($this->box[$box]);
			$msg = sprintf("msg%04d", $i - 1);
		}
		$ordinary='message_start';
		if (isset($_GET['state'])) {
			$override = $_GET['state'].'_override';
			if (method_exists($this, $override)) {
				$this->$override($box, $msg, $i);
				return;
			}
			if (method_exists($this, $_GET['state'])) {
				$ordinary = $_GET['state'];
			}
		}
		if ($box == 'INBOX') {
			$this->back = $this->url.'?action=message&box='.$box.'&msg='.$msg.'&state=move_to&dst=Old';
		} else {
			$this->back = $this->url.'?action=mailbox&box='.$box;
		}
		$c = new ConfigFile($this->path.'/'.$box.'/'.$msg.'.txt');
		$m = $c->section('message');
		$d = date('D, j M Y h:i:s A T Y', $m['origtime']);
		$this->displayObject(new TextScreen());
		$this->do->addText('Received: '.$d);
		$this->do->addText('From: '.$m['callerid']);
		$this->do->addText('Length '.$m['duration'].' seconds');
		$this->$ordinary($box, $msg, $i);
	}

	function mailbox() {
		$box = $_GET['box'];
		$this->title = $this->mapping[$box];
		$list = new VoicemailListManager($this, $this->url.'?action=mailbox&box='.$box);
		$list->manager($box, $this->url.'?action=message&box='.$box.'&msg=');
	}

	function start() {
		$this->displayObject(new TextScreen());
		$softkey = 1;
		foreach ($this->mapping as $k => $v) {
			$c = count($this->box[$k]);
			$this->do->addText($c.' '.$v);
			if ($c) {
				$this->do->addSoftkey($softkey, $v,  $this->url.'?action=mailbox&box='.$k);
			}
			$softkey++;
		}
	}
}
?>
