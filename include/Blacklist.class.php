<?php
##
# Copyright 2013 by James Bottomley
##
# Blacklist handling class
##
require_once('BaseAastra.class.php');
require_once('TextScreen.class.php');
require_once('AastraIPPhoneInputScreen.class.php');

class Blacklist extends BaseAastra {

	var $lastnumber;
	var $name;
	var $blacklist;
	var $whitelist;

	private function __initvar() {
		$this->lastnumber = $this->asm->database_get('lastnumber', '1');
		$this->blacklist = $this->asm->database_get('blacklist', $this->lastnumber);
		$this->whitelist = $this->asm->database_get('whitelist', $this->lastnumber);
	}

	function __construct() {
		parent::__construct();
		$this->title = 'Blacklist/Whitelist';
		$this->__initvar();
	}

	function del_entry($list) {
		if (isset($_GET['yesno'])) {
			if ($_GET['yesno'] == 'yes') {
				$this->asm->database_del($list, $this->lastnumber);
			}
			return;
		}
		$this->displayObject(new TextScreen());
		$this->do->SetText('Are you sure you want to remove '.$this->lastnumber.' from the '.$list.'?');
		$this->yesno();
	}

	function del_black() {
		$this->del_entry('blacklist');
	}

	function black() {
		if (isset($_GET['yesno'])) {
			if ($_GET['yesno'] == 'yes') {
				$this->asm->database_del('whitelist',$this->lastnumber);
				$this->asm->database_put('blacklist',$this->lastnumber,'1');
			}
			return;
		}
		$this->displayObject(new TextScreen());
		$this->do->SetText('Are you sure you want to blacklist '.$this->lastnumber.'?');
		$this->yesno();
	}

	function newnum() {
		if (isset($_GET['num'])) {
			$this->asm->database_put('lastnumber','1',$_GET['num']);
			$this->__initvar();
			$this->start();
			return;
		}
		$this->displayObject(new AastraIPPhoneInputScreen());
		$this->do->setParameter('num');
		$this->do->setType('number');
		$this->do->setPrompt('Enter number:');
		$this->do->setURL($this->fullurl);
	}

	function del_white() {
		$this->del_entry('whitelist');
	}

	function white() {
		if (isset($_GET['description'])) {
			$this->asm->database_del('blacklist',$this->lastnumber);
			$this->asm->database_put('whitelist',$this->lastnumber,'"'.$_GET['description'].'"');
			return;
		}
		$this->displayObject(new AastraIPPhoneInputScreen());
		$this->do->setParameter('description');
		$this->do->setType('string');
		$this->do->setPrompt('Enter description for '.$this->lastnumber.':');
		$this->do->setURL($this->fullurl);
		if (isset($_GET['current'])) {
			$this->do->setDefault($_GET['current']);
		}
	}

	function start() {
		$this->displayObject(new TextScreen());
		$text = 'The last number was '.$this->lastnumber;
		if ($this->blacklist) {
			$text .= ' which is already blacklisted';
			$this->do->addSoftkey('1', 'Remove Blacklist', $this->url.'?action=del_black');
		} else {
			$this->do->addSoftkey('1', 'Blacklist', $this->url.'?action=black');
		}
		if ($this->whitelist) {
			$text .= ' which is already in the whitelist as'.self::nbsp.'"'.$this->whitelist.'"';
			$this->do->addSoftkey('2', 'Remove Whitelist', $this->url.'?action=del_white');
			$this->do->addSoftkey('4', 'Edit Whitelist', $this->url.'?action=white&current='.$this->whitelist);
		} else {
			$this->do->addSoftkey('2', 'Whitelist', $this->url.'?action=white');
		}
		$this->do->SetText($text);
		$this->do->addSoftkey('3', 'New Number', $this->url.'?action=newnum');
	}
}
?>
