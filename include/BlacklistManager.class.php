<?php
##
# Copyright 2013 by James Bottomley
##
# Blacklist handling class
##
require_once('Blacklist.class.php');
require_once('DatabaseListManager.class.php');

class BlacklistManager extends Blacklist {
	function white_manager() {
		$this->title = 'Whitelist';
		$list = new DatabaseListManager($this, $this->url.'?action=white_manager');
		$list->setTitle($this->title);
		$list->manager('whitelist', true,
				$this->url.'?action=newnum&num=');
	}

	function black_manager() {
		$this->title = 'Blacklist';
		$list = new DatabaseListManager($this, $this->url.'?action=black_manager');
		$list->setTitle($this->title);
		$list->manager('blacklist', false,
			       $this->url.'?action=newnum&num=');
	}

	function start() {
		parent::start();
		$this->do->addSoftkey('6', 'Blacklist Manager', $this->url.'?action=black_manager');
		$this->do->addSoftkey('7', 'Whitelist Manager', $this->url.'?action=white_manager');
	}
}
?>
