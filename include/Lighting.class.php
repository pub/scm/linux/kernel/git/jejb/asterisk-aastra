<?php
require_once('BaseAastra.class.php');
require_once('VoicemailListManager.class.php');
require_once('AastraIPPhoneExecute.class.php');
require_once('Homeseer.class.php');

class Lighting extends BaseAastra {

	public $hs;

	const on = 'Icon:CircleGreen';
	const off = 'Icon:CircleBlue';
	const conffile = '/etc/asterisk/lighting.ini';

	private $lights;
	private $allowed;

	function __construct() {
		parent::__construct();
		$this->title = 'Lighting Control';
		$this->hs = new Homeseer();
		$this->lights = array();
		$conf = parse_ini_file(self::conffile, TRUE);
		$this->hs->set_config($conf['homeseer']);
		$this->lights = $conf[$this->user]['lights'];
		$this->allowed = array_flip($this->lights);
	}

	function get_icon($val) {
		$onoff = $this->hs->get_status($val);

		if ($onoff) {
			return self::on;
		}
		return self::off;
	}

	private function _render() {
		$this->displayObject(new TextScreen());

		for ($i = 0; $i < sizeof($this->lights); $i++) {
			$val = $this->lights[$i];
			if (!$this->hs->is_mapped($val))
				throw new Exception('Undefined light '.$val);
			$this->do->addSoftkey($i+1, $val, $this->url.'?action=toggle&dev='.$val, $this->get_icon($val));
		}
	}

	function start() {
		$this->_render();
	}

	function toggle() {
		$dev = $_GET['dev'];
		if (!isset($this->allowed[$dev])) {
			throw new Exception('Unknown Device '.$dev);
		}
		$onoff = $this->hs->get_status($dev);

		if ($onoff == 0) {
			$this->hs->set_status($dev, 99);
			$this->hs->set_status($dev, 255);
		} else {
			$this->hs->set_status($dev, 0);
		}

		$this->_render();
	}

	function init() {
		parent::init();

		$this->oldstat[69] = 99;
	}
}
?>
