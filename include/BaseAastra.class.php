<?php
##
# Copyright 2013 by James Bottomley
##
# Base abstract class for building up XML actions
##
require_once('AastraAsterisk.class.php');

abstract class BaseAastra {
	var $asm;
	var $user;
	var $url; /* without query strings */
	var $fullurl;
	var $title;
	var $do = null;
	var $back = null;
	const nbsp = "\xa0";

	static final function exception_handler($e) {
		require_once('AastraIPPhoneTextScreen.class.php');

		$object=new AastraIPPhoneTextScreen();
		$object->setDestroyOnExit();
		$object->SetTitle("Error");
		$object->SetText($e->getMessage());
		$object->output();
	}

	function __construct() {
		header('Content-type: text/xml');
		set_exception_handler('BaseAastra::exception_handler');
		$this->asm=new AGI_AsteriskManager();
		$this->asm->connect();
		$this->url = 'http';
		if (isset($_SERVER['HTTPS'])) {$this->url .= 's';}
		$this->url .= '://'.$_SERVER["SERVER_NAME"];
		$pos = strpos($_SERVER["REQUEST_URI"], '?');
		if ($pos !== false) {
			$baseurl = substr($_SERVER['REQUEST_URI'], 0, $pos);
		} else {
			$baseurl = $_SERVER['REQUEST_URI'];
		}
		if ($_SERVER["SERVER_PORT"] != "80") {
			$this->url .= ":".$_SERVER["SERVER_PORT"];
		}
		$this->fullurl = $this->url . $_SERVER['REQUEST_URI'];
		$this->url .= $baseurl;
		$this->user = $this->asm->get_user();
		$this->back = $this->url;
	}

	function init() {
		if (isset($_GET['action'])) {
			$this->{$_GET['action']}();
		} else {
			$this->back = null;
			$this->start();
		}
		if (!$this->do) {
			require_once('AastraIPPhoneExecute.class.php');
			$this->do = new AastraIPPhoneExecute();
			$this->do->addEntry('');
			$this->do->setTriggerDestroyOnExit();
		}
		$this->do->setTitle($this->title);
		$this->do->output();
	}

	const validity = 600; # 600 seconds is 10 minutes

	function getActionHandler() {
		if (!isset($_GET['app'])) {
			throw new Exception('No App defined in action handler');
		}
		$app = $_GET['app'];
		$t = $this->asm->database_get('actionhandler', $app.'-'.$this->user.'-timestamp');
		if ($t > time() + self::validity) {
			return null;
		}
		return $this->asm->database_get('actionhandler',$app.'-'.$this->user);
	}

	function setActionHandler($app, $url) {
		$this->asm->database_put('actionhandler', $app.'-'.$this->user, $url);
		$this->asm->database_put('actionhandler', $app.'-'.$this->user.'-timestamp', time());
	}

	function delActionHandler($app) {
		$this->asm->database_del('actionhandler', $app.'-'.$this->user);
		$this->asm->database_del('actionhandler', $app.'-'.$this->user.'-timestamp');
	}

	function yesno() {
		$this->do->addSoftkey('1', 'Yes', $this->fullurl.'&yesno=yes');
		$this->do->addSoftkey('2', 'No', $this->fullurl.'&yesno=no');
	}

	function displayObject($o) {
		$this->do = $o;
		$o->setDestroyOnExit();
		if($this->back) {
			$o->setCancelAction($this->back);
		}
	}

	abstract function start();
}
