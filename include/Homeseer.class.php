<?php


class Homeseer {
	private $baseurl;
	private $user;
	private $passwd;
	private $ch;
	private $map;

	function __construct() {
		$this->ch = curl_init();
		curl_setopt($this->ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
	}

	private function get_url($url) {
		curl_setopt($this->ch, CURLOPT_URL,
			    $this->baseurl.$url);
		$json = curl_exec($this->ch);
		$err = curl_error($this->ch);
		if ($err) {
			throw new Exception($err);
		}
		return $json;
	}

	private function get_ref($dev) {
		$ref = $this->map[$dev];

		if (!isset($ref))
			throw new Exception('Unknown Light: '.$dev);

		if (is_array($ref))
			return $ref;

		return [ $ref ];
	}

	function set_config($conf) {
		if (!isset($conf['url']))
			throw new Exception('No url set in config');
		if (!isset($conf['user']))
			throw new Exception('No user set in config');
		if (!isset($conf['password']))
			throw new Exception('No password set in config');
		if (!isset($conf['map']))
			throw new Exception('No map set in config');
		$this->baseurl = $conf['url'];
		curl_setopt($this->ch, CURLOPT_USERPWD, $conf['user'].":".$conf['password']);
		$this->map = $conf['map'];
		if (isset($conf['multi'])) {
			foreach ($conf['multi'] as $m) {
				if (!isset($conf[$m]))
					throw new Exception ('Homeseer config is missing multi '.$m.' definition');
				$this->map[$m] = $conf[$m];
			}
		}
	}

	private function get_status_ref($ref) {
		$json = $this->get_url('/JSON?request=getstatus&ref='.$ref);
		$obj = json_decode($json);
		$val = $obj->{'Devices'}[0]->{'value'};
		if (!isset($val))
			throw new Exception('Failed to get homeseer data for '.$dev);
		return $val;
	}

	function get_status($dev) {
		$ref = $this->get_ref($dev);
		$val = 0;
		foreach ($ref as $r) {
			$val += $this->get_status_ref($r);
		}
		return $val;
	}

	function set_status($dev, $val) {
		$ref = $this->get_ref($dev);
		foreach ($ref as $r) {
			$this->get_url('/JSON?request=controldevicebyvalue&ref='.$r."&value=".$val);
		}
	}

	function is_mapped($dev) {
		return isset($this->map[$dev]);
	}
}
