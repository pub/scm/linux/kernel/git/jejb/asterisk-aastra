<?php
##
# Copyright 2013 by James Bottomley
##
# Class implementing a wrapper for managing lists in the database
##
require_once('BaseList.class.php');
require_once('ConfigFile.class.php');

class VoicemailListManager extends BaseList {
	##
	# hacking inner classes.  This list manager should really be
	# the inner class of the overall object, so it has access
	# to the known BaseAastra functions.  However, hack it like this
	# so $this->outer is a pointer to the outer class
	##
	var $outer;

	function __construct($outer, $url) {
		$this->outer = $outer;
		parent::__construct($url);
	}

	##
	# The manager() function takes the name of the database list
	# the 
	function manager($box, $selecturl) {
		$this->data = array();
		$this->files = array_keys($this->outer->box[$box]);
		$this->path = $this->outer->path.'/'.$box;
		sort($this->files);
		foreach($this->files as $v) {
			preg_match('/^msg(\d*)$/', $v, $m);
			$i = intval($m[1]) + 1;
			$c = new ConfigFile($this->path.'/'.$v.'.txt');
			$m = $c->section('message');
			$a = array();
			$d = date('D, j M Y h:i:s A T Y', $m['origtime']);
			$a[0] = $i.': '.$d;
			$a[1] = $selecturl.$v;
			$this->data[] = $a;
		}
		$this->outer->displayObject($this);
		$this->build();
	}
}
?>
