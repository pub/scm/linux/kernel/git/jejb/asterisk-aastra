<?php
##
# Copyright 2013 by James Bottomley
##
# Class implementing a wrapper for List objects
##
require_once('TextScreen.class.php');

class BaseList extends TextScreen {
	var $data;
	var $page;
	var $title;
	var $url;

	function list_scroll_up() {
	}

	function __construct($url) {
		$this->page = 0;
		if (isset($_GET['page'])) {
			$this->page = $_GET['page'];
		}
		$this->url = $url;
		parent::__construct();
	}

	function setTitle($title) {
		$this->title = $title;
	}

	function build() {
		$pages = intval((count($this->data)+3)/$this->_phone['entries']);
		$down = $this->page + 1 == $pages ? null : $this->url.'&page='.($this->page + 1).'&index='.$this->_phone['entries'];
		$up = $this->page == 0 ? null : $this->url.'&page='.($this->page - 1).'&index=0';
		if ($pages) {
			parent::setTitle($this->title.' Page: '.($this->page+1).'/'.$pages);
		} else {
			parent::setTitle($this->title.' List Empty');
		}
		$this->setStyle('none');
		if ($up) {
			$this->addSoftKey(1, 'Prev', $up);
			$this->setScrollUp($up);
		}
		if ($down) {
			$this->addSoftKey(2, 'Next', $down);
			$this->setScrollDown($down);
		}
		if (isset($_GET['index'])) {
			$this->setDefaultIndex($_GET['index']);
		}
		$start = $this->page * $this->_phone['entries'];
		$end = ($this->page + 1) * $this->_phone['entries'];
		if ($end > count($this->data)) {
			$end = count($this->data);
		}
		for($i = $start; $i < $end; $i++) {
			$a = $this->data[$i];
			$this->addEntry($a[0], $a[1]);
		}
	}
}
