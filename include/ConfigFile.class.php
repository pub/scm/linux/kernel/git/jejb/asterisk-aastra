<?php
##
# Copyright 2013 by James Bottomley
##
# Class implementing a wrapper for managing asterisk config files
##
class ConfigFile {
	var $file;
	var $entries;

	function __construct($file) {
		$this->file = $file;
		$this->entries = array();
		$raw = file($file, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
		foreach($raw as $line) {
			if (preg_match("/^\s*[#;]/", $line)) {
				continue;
			}
			if (preg_match("/^\s*\[(.*)\]/", $line, $m)) {
				$section = $m[1];
				$this->entries[$section] = array();
			}
			if (preg_match("/^\s*(\S+)\s*=>*\s*(.*)/", $line, $m)) {
				$var = $m[1];
				$val = $m[2];
				if (preg_match("/^(.*)\s*[#;]/", $line, $m)) {
					$val = $m[1];
				}
				$this->entries[$section][$var] = $val;
			}
		}	       
	}

	function section($sec) {
		if (!isset($this->entries[$sec])) {
			return null;
		}
		return $this->entries[$sec];
	}

	function value($sec, $var) {
		if (!isset($this->entries[$sec])) {
			return null;
		}
		if (!isset($this->entries[$sec][$var])) {
			return null;
		}
		return $this->entries[$sec][$var];
	}
}
?>
