<?php
################################################################################
# Aastra XML API Classes - AastraIPPhoneStatusEntry
# Copyright Aastra Telecom 2007-2010
#
# Internal class for AastraIPPhoneStatus object.
################################################################################

class AastraIPPhoneStatusEntry extends AastraIPPhone {
	var $_index;
	var $_message;
       var $_type='';
       var $_timeout=0;

	function AastraIPPhoneStatusEntry($index, $message, $type='', $timeout=NULL)
	{
		$this->_index = $index;
		$this->_message = $this->convert_high_ascii($message);
		$this->_type = $type;
		$this->_timeout = $timeout;
	}

	function render()
	{
		$index = $this->escape($this->_index);
		$message = $this->escape($this->_message);
		$type = $this->escape($this->_type);
		$timeout = $this->_timeout;
		$xml = "<Message index=\"{$index}\"";
		if ($type!='') 
			{
			$xml .= " type=\"{$type}\"";
			if ($timeout!=NULL) $xml .= " Timeout=\"{$timeout}\"";
			}
		$xml .= ">{$message}</Message>\n";
		return($xml);
	}
}
?>
