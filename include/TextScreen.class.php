<?php
##
# Copyright 2013 by James Bottomley
##
# Phone Independent text screen handling class
##
require_once('AastraIPPhoneTextScreen.class.php');
require_once('AastraIPPhoneFormattedTextScreen.class.php');
require_once('AastraIPPhoneTextMenu.class.php');

class TextScreen {
	var $_phone = array(
		# notional: the 6739i uses a variable width font
		'width' => 38,
		'height' => 14,
		'entries' => 4,
		'formatted' => true,
		'unitscrollbug' => true,
	);
	var $_softkeys = array();
	var $_entries = array();
	var $_text = array();
	var $_title;
	var $_destroyOnExit;
	var $_cancelAction;
	var $_style;
	var $_scrollUp;
	var $_scrollDown;
	var $_defaultIndex;
	var $_allowDrop;

	function __construct() {
	}

	function addSoftKey($index, $label, $url, $icon = null) {
		$this->_softkeys[$index] = array(
			'label' => $label,
			'url' => $url,
			'icon' => $icon,
		);
	}

	function setTitle($title) {
		$this->_title = $title;
	}

	function setStyle($style) {
		$this->_style = $style;
	}

	function setCancelAction($cancelAction) {
		$this->_cancelAction=$cancelAction;
	}

	function setDestroyOnExit() {
		$this->_destroyOnExit = 'yes';
	}

	function setAllowDrop() {
		$this->_allowDrop = 'yes';
	}

	function setDefaultIndex($index) {
		$this->_defaultIndex = $index;
	}

	function setScrollUp($uri) {
		$this->_scrollUp = $uri;
	}

	function setScrollDown($uri) {
		$this->_scrollDown = $uri;
	}

	function addEntry($name, $url, $selection = null, $icon = null,
			$dial = null, $line = null) {
		$this->_entries[] = array(
			'name' => $name,
			'url' => $url,
			'selection' => $selection,
			'icon' => $icon,
		);
	}

	function setText($text) {
		$this->_text = array($text);
	}

	function addtext($text) {
		$this->_text[] = $text;
	}

	function output() {
		if (count($this->_entries) == 0) {
			if ($this->_phone['formatted']) {
				$o = new AastraIpPhoneFormattedTextScreen();
			} else {
				$o = new AastraIPPhoneTextScreen();
			}
		} else {
			$o = new AastraIPPhoneTextMenu();
		}
		$o->setTitle($this->_title);
		if ($this->_destroyOnExit) {
			$o->setDestroyOnExit();
		}
		if ($this->_allowDrop) {
			$o->setAllowDrop();
		}
		if ($this->_phone['unitscrollbug'] && method_exists($o, 'setUnitScroll')) {
			$o->setUnitScroll();
		}
		if ($this->_cancelAction) {
			$o->setCancelAction($this->_cancelAction);
		}
		if ($this->_style && !$this->_phone['formatted']) {
			$o->setStyle($this->_style);
		}
		if ($this->_scrollUp) {
			$o->setScrollUp($this->_scrollUp);
		}
		if ($this->_scrollDown) {
			$o->setScrollDown($this->_scrollDown);
		}
		if ($this->_defaultIndex) {
			$o->setDefaultIndex($this->_defaultIndex);
		}
		foreach($this->_entries as $e) {
			$name = $e['name'];
			$url = $e['url'];
			$selection = $e['selection'];
			$icon = $e['icon'];
			$o->addEntry($name, $url, $selection, $icon);
		}
		$icons=1;
		foreach($this->_softkeys as $k=>$e) {
			$label = $e['label'];
			$url = $e['url'];
			$icon = $e['icon'];
			if ($icon) {
				$o->addSoftkey($k, $label, $url, $icons);
				$o->addIcon($icons++, $icon);
			} else {
				$o->addSoftkey($k, $label, $url, NULL);
			}
		}
		$o->setTitle($this->_title);
		if (count($this->_entries) != 0) {
			#do nothing
		} else if ($this->_phone['formatted']) {
			foreach($this->_text as $t) {
				$s = preg_split('/\n/',wordwrap($t, $this->_phone['width'], "\n\xa0\xa0"));
				foreach($s as $l) {
					$o->addLine($l);
				}
			}
		} else if (count($this->_text) == 0) {
			$o->setText($this->_text[0]);
		} else {
			$text = '';
			foreach($this->_text as $t) {
				$c = strlen($t) % $this->_phone['width'];
				$c = $c ? $this->_phone['width'] - $c : 0;
				error_log("length of t is ".strlen($t).' c='.$c);
				$text .= $t . str_repeat("\xa0", $c).'|';
			}
			$o->setText($text);
		}
		$o->output();
	}
}
