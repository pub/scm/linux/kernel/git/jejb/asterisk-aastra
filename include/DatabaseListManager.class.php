<?php
##
# Copyright 2013 by James Bottomley
##
# Class implementing a wrapper for managing lists in the database
##
require_once('BaseList.class.php');

class DatabaseListManager extends BaseList {
	##
	# hacking inner classes.  This list manager should really be
	# the inner class of the overall object, so it has access
	# to the known BaseAastra functions.  However, hack it like this
	# so $this->outer is a pointer to the outer class
	##
	var $outer;

	function __construct($outer, $url) {
		$this->outer = $outer;
		parent::__construct($url);
	}

	##
	# The manager() function takes the name of the database list
	# the 
	function manager($list, $showval, $selecturl) {
		$this->data = array();
		$l = $this->outer->asm->database_show($list);
		if ($showval) {
			asort($l);
		} else {
			$l = array_keys($l);
			sort($l);
			$l = array_flip($l);
		}
		foreach($l as $k => $v) {
			$a = array();
			$kk = preg_replace('/\/'.$list.'\//', '', $k);
			if ($showval) {
				$a[] = $v." ".$kk;
			} else {
				$a[] = $kk;
			}
			$a[] = $selecturl.$kk;
			$this->data[] = $a;
		}
		$this->outer->displayObject($this);
		$this->build();
	}
}
?>
