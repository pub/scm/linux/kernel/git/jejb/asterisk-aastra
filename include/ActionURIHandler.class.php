<?php
##
# Copyright 2013 by James Bottomley
##
# actionuri handler class
##
require_once('BaseAastra.class.php');
require_once('AastraIPPhoneExecute.class.php');

class ActionURIHandler extends BaseAastra {
	function start() {
		$this->displayObject(new AastraIPPhoneExecute());
		$uri = $this->getActionHandler();
		if (!$uri) {
			##
			# do nothing.  This is the most common case
			# because mostly nothing wants to intercept
			# the actions
			##
			$this->do->addEntry(" ");
		} else {
			$this->do->addEntry($uri);
		}
	}
}
?>
