prefix := /usr/local
php_prefix := $(prefix)/php
web_prefix := $(prefix)/var/www/asterisk-aastra
doc_prefix := $(prefix)/usr/share/doc/asterisk-aastra
etc_prefix := /etc/asterisk


base-files = \
	include/AastraAsterisk.class.php \
	include/AastraIPPhone.class.php \
	include/AastraIPPhoneConfiguration.class.php \
	include/AastraIPPhoneConfigurationEntry.class.php \
	include/AastraIPPhoneDirectory.class.php \
	include/AastraIPPhoneDirectoryEntry.class.php \
	include/AastraIPPhoneExecute.class.php \
	include/AastraIPPhoneExecuteEntry.class.php \
	include/AastraIPPhoneFormattedTextScreen.class.php \
	include/AastraIPPhoneFormattedTextScreenEntry.class.php \
	include/AastraIPPhoneGDImage.class.php \
	include/AastraIPPhoneIconEntry.class.php \
	include/AastraIPPhoneImageMenu.class.php \
	include/AastraIPPhoneImageMenuEntry.class.php \
	include/AastraIPPhoneImageScreen.class.php \
	include/AastraIPPhoneInputScreen.class.php \
	include/AastraIPPhoneInputScreenEntry.class.php \
	include/AastraIPPhone.php \
	include/AastraIPPhoneScrollableDirectory.class.php \
	include/AastraIPPhoneScrollableTextMenu.class.php \
	include/AastraIPPhoneScrollHandler.php \
	include/AastraIPPhoneSoftkeyEntry.class.php \
	include/AastraIPPhoneSPImage.class.php \
	include/AastraIPPhoneStatus.class.php \
	include/AastraIPPhoneStatusEntry.class.php \
	include/AastraIPPhoneTextMenu.class.php \
	include/AastraIPPhoneTextMenuEntry.class.php \
	include/AastraIPPhoneTextScreen.class.php \
	include/BaseAastra.class.php \
	include/BaseList.class.php \
	include/DatabaseListManager.class.php \
	include/ConfigFile.class.php \
	include/TextScreen.class.php

base-doc-files = \
	etc/apache-asterisk \
	README

blacklist-files = \
	include/Blacklist.class.php \
	include/BlacklistManager.class.php

blacklist-doc-files = \
	etc/extensions-blacklist.conf

actionuri-files = \
	include/ActionURIHandler.class.php

voicemail-files = \
	include/Voicemail.class.php \
	include/VoicemailListManager.class.php

voicemail-doc-files = \
	etc/extensions-voicemail.conf

lighting-files = \
	include/Lighting.class.php \
	include/Homeseer.class.php \

packages := blacklist actionuri

clean:
	@echo

all:
	@echo

install: install-base $(patsubst %,install-%,$(packages))

install-php:
	install -d $(php_prefix)

install-web:
	install -d $(web_prefix)

install-doc:
	install -d $(doc_prefix)

install-base: install-php install-doc
	install -d $(prefix)/etc/asterisk/manager.d
	install -m 0644 etc/asterisk-aastra.conf $(prefix)/etc/asterisk/manager.d/
	install -m 0644 $(base-files) $(php_prefix)
	install -m 0644 $(base-doc-files) $(doc_prefix)

install-blacklist: install-php install-web install-doc
	install -m 0644 $(blacklist-files) $(php_prefix)
	install -m 0644 blackwhitelist.php $(web_prefix)
	install -m 0644 $(blacklist-doc-files) $(doc_prefix)

install-actionuri: install-php install-web
	install -m 0644 $(actionuri-files) $(php_prefix)
	install -m 0644 actionuri.php $(web_prefix)

install-voicemail: install-php install-web install-doc
	install -m 0644 $(voicemail-files) $(php_prefix)
	install -m 0644 voicemail.php $(web_prefix)
	install -m 0644 $(voicemail-doc-files) $(doc_prefix)

install-lighting: install-php install-web install-doc
	install -m 0644 $(lighting-files) $(php_prefix)
	install -m 0644 lighting.php $(web_prefix)
	install -m 0644 etc/lighting.ini $(etc_prefix)
